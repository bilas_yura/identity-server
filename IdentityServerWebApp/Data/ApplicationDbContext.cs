﻿using IdentityServer4.EntityFramework.Options;
using IdentityServerWebApp.Models;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace IdentityServerWebApp.Data
{
    public class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions options, 
            IOptions<OperationalStoreOptions> operationalOptions) : 
                base(options, operationalOptions) { }
    }
}
