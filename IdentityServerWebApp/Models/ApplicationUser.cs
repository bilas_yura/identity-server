﻿using Microsoft.AspNetCore.Identity;

namespace IdentityServerWebApp.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}
